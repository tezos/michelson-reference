all:
	make -C michelson_reference all
	@echo "Documentation is generated in file://`pwd`/michelson_reference/docs/index.html"

clean:
	make -C michelson_reference clean

touch-ott:
	touch michelson_reference/michelson.ott

check-committed: touch-ott michelson_reference/michelson.json michelson_reference/michelson_embed.tex
	@if git diff --exit-code ; then\
		echo "\n[OK] michelson.json and michelson_embed.tex are up-to-date.\n";\
	else \
		echo "\n[ERROR] Some committed generated file is not up-to-date with michelson.ott\n";exit 1;\
	fi

build-docker: clean
	docker build . -t tezos/michelson_reference

run-docker: build-docker
	docker run -d -p 80:80 tezos/michelson_reference
	@echo "Documentation hosted by Docker container is available in http://localhost"

.PHONY: test
test:
	poetry run python -m pytest -x tests/

lint:
	make -C tests lint_all typecheck

.PHONY: validate-schema
validate-schema:
	poetry run python michelson_reference/validate-json-schema.py \
		michelson_reference/michelson-meta-schema.json \
		michelson_reference/michelson-meta.yaml

.PHONY: validate-meta
validate-meta:
	poetry run python michelson_reference/validate_meta.py michelson_reference/michelson-meta.yaml

build-docker-test:
	docker build -f Dockerfile_test . -t michelson_reference_test

.PHONY: docker-test
docker-test: build-docker-test
	docker run michelson_reference_test
