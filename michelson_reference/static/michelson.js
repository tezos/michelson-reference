function addTypemap(pre, typemap) {
    var cl = pre.clone();

    $(pre).find('span').each(function () {
        var tgt = $(this);
        var code = pre.text();
        var idx = $(this).parent('pre').children().index(tgt);

        // find point corresponding to the beginning of this primitive
        var ch = $(cl.children().get(idx))
        var origText = ch.text()
        ch.text('<<NEEDLE>>')
        var idxTxt = cl.text().indexOf("<<NEEDLE>>")
        ch.text(origText)

        // find the most specific type in the typemap for this point, e.g.
        // the type assigned to the location with smallest distance between
        // start and end.
        var min = false;
        var title = false;
        typemap.forEach(function (el) {
            // console.log('el', el);
            var len = el.location.location.stop.point - el.location.location.start.point;
            if (el.location.location.start.point <= idxTxt &&
                idxTxt <= el.location.location.stop.point &&
                (min === false || len < min)) {
                var before = el.before.length ? el.before.join(' : ') + ' : []' : '[]';
                var after = el.after.length ? el.after.join(' : ') + ' : []' : '[]';
                title = before + ' → ' + after;
                min = len;
            }
        });
        if (title) {
            tgt.attr('title', title)
        }
    })
}

$(function () {
    $('.highlight > pre').hover(function (e) {
        var pre = $(this)
        var code = pre.text();
        if (!pre.is('.type-checked')) {
            pre.addClass('type-checked');
            $.ajax({
                url: "https://tezos-lang-server.tzalpha.net/typecheck_code",
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                processData: false,
                data: JSON.stringify({code: code}),
                success: function (typecheckResults) {
                    addTypemap(pre, typecheckResults.typemap)
                },
                error: function(){
                    console.log('error', arguments);
                }
            });
        }
    });

    $(window).on('hashchange', function () {
        if (window.location.hash == "") {
            console.log(window.location.hash);
            $("#right > .bottom").scrollTop(0);
        }
    });
});
