{% import 'macros.html' as macros %}

{{ macros.linkable_header(2, 'Introduction', 'introduction', 'section') }}

<p>This is a reference manual of the Michelson language used for smart contract
programming for the Tezos blockchain. It contains a reference of the
types and instructions of the language. For more information about the Michelson language, such as its design rationale and how it fits in the blockchain, deprecated instructions, macros, and other aspects, see the <a href="http://tezos.gitlab.io/active/michelson.html">Michelson language</a> page in the Tezos technical documentation.</p>

<p>The simplest contract is the contract for which the parameter and storage are all of type unit. This empty contract is as follows:</p>

{{ macros.show_example(try_michelson_url, "parameter unit;\nstorage unit;\ncode {CDR; NIL operation; PAIR};", "-", "Unit", "Unit") }}

<p>The examples used throughout the reference can be executed in the
browser using the <a href="{{try_michelson_url}}">Try Michelson</a>
interpreter. Just click "try it!" in the upper-right corner of the
example to load it in the interpeter.</p>

<p>In addition, the stack type of each location of the examples can be displayed
by hovering the mouse over that location.</p>

{{ macros.linkable_header(2, 'Execution environment', 'execution', 'section') }}


<p>A well-typed Michelson program implements a function which transforms a pair of Michelson
values representing the transaction parameter and the contract storage, and
returns a pair containing a list of internal operations and the new storage.
Some Michelson instructions make implicit use of data from an execution
environment, derived from the current state of the blockchain (the context),
from the transaction that triggered the contract, and from the block
containing this transaction. These instructions are categorized under
<a href="#instructions-blockchain">blockchain operations</a>.</p>

{{ macros.linkable_header(2, 'Terminology', 'terminology', 'section') }}

<dl>
<dt>Instruction</dt>
<dd>Instructions refer to Michelson primitives such as <code>ADD</code>.
    Not to be confused with operations. </dd>
<dt>Operation</dt>
<dd>The final stack after a contract execution is a pair containing a new storage and a list of operations.
    An operation is either a transfer, an account creation, a delegation or the
    emission of an event.</dd>
<dt>Numerical type</dt>
<dd>Refers to any of the numerical types of Michelson:
    <code>int</code>,
    <code>nat</code>,
    <code>timestamp</code> or
    <code>mutez</code>.
    Numerical values are values of one these types.
<dt>Sequence type</dt>
<dd>Refers to any of the two sequence types of Michelson: <code>string</code> or <code>bytes</code>.
</dd>
<dt>Structural type</dt>
<dd>Refers to any of the structural types of Michelson:
    <code>list</code>,
    <code>set</code>,
    <code>map</code> or
    <code>big_map</code>.
    Structural values are values of one these types.
</dd>
<dt>Argument</dt>
<dd>Michelson instructions may be indexed by arguments.
    For instance, the
    arguments of <code>PUSH nat 3</code> are <code>nat</code> and <code>3</code>.
    The argument of <CODE>DIP 2</CODE> is <code>2</code>.
    Not to be confused with operands and parameters.
</dd>
<dt>Operand</dt>
<dd>Refers to stack values consumed by instructions.
    Not to be confused with arguments and parameters.
</dd>
<dt>Parameter</dt>
<dd>Parameter may refer to three things.
    First, each contract has a parameter type and when called, takes a parameter value of this type.
    Second, some Michelson data types, such as <code>list</code> are parametric.
    For instance, a list of integer <code>list nat</code> is an instance of the list type with
    the type parameter instantiated to <code>nat</code>.
    Finally, the input type of a <code>lambda</code>.
    Not to be confused with arguments and operands.
</dd>
<dt>Return value</dt>
<dd>Refers either to the stack values produced by an instruction, or the return value of a <code>lambda</code>.
</dd>

<dt>Transaction</dt>

<dd>The execution of a contract is triggered by a blockchain operation
called a transaction, which is part of a block or is emitted by the execution
of another contract or rollup. A transaction has parameters
which are made available to the contracts in two different ways. The
argument of the transaction (a Michelson value) is passed to the contract as
the right side of a pair value, the only element in the initial stack.
Other parameters of the transaction (such as its amount, the implicit account
or contract that triggered it), and data from the block containing the
transaction (its timestamp) can be accessed by specific instructions. </dd>

<dt>Context</dt>

<dd>The context is a value representing the state of the blockchain after
the application of an operation. A contract execution may access data from the
context over which the execution is triggered. It has access to the
previous storage of the contract as the left side of a pair value, the only
element in the initial stack. Besides, some instructions use or extract
information from the context (current contract balance, contracts type).
</dd>

<dt>Storage</dt>
<dd> The storage of a contract is a Michelson value that represents it state.
Initial storage is provided at contract deployement (a.k.a. origination).
Subsequently, transactions trigger contract execution, in which a new store
is computed, based notably on the previous storage and the transaction argument.
</dd>

</dl>

{{ macros.linkable_header(2, 'Typing and Semantics Rules', 'typing-and-semantics-rules', 'section') }}

<p>Michelson is a strongly typed language, which greatly participates in its safety.<br/>
A <em>type-checking</em> process forbids any Michelson script to be registered on the blockchain if one of its instructions does not operate on operands and a stack of the expected types.</p>

<p>In the <a href="#instructions">Instructions</a> section, the description of an instruction will always come with <em>typing rules</em> characterizing the typing conditions that its operands and the stack must satisfy before applying the instruction, and the resulting stack type after the execution of the instruction.<br/>
<em>Semantics rules</em> will also precisely specify the values manipulated by the instruction and its result.</p>

<p>Typing as well as semantics rules have the following form:
<div class="rules typing">
    <div class="rule-box">
<table class="display dcenter"><tr style="vertical-align:middle"><td class="dcell"><table class="display"><tr style="vertical-align:middle"><td class="dcell"><table class="display"><tr><td class="dcell" style="text-align:center"><table class="cellpading0" style="border-spacing:6px;border-collapse:separate;"><tr><td style="text-align:left;white-space:nowrap">  <span style="font-style:italic">condition</span><sub>1</sub></td></tr>
<tr><td style="text-align:left;white-space:nowrap">  ...</td></tr>
<tr><td style="text-align:left;white-space:nowrap">  <span style="font-style:italic">condition</span><sub>n</sub></td></tr>
</table></td></tr>
<tr><td class="hrule"></td></tr>
<tr><td class="dcell" style="text-align:center">conclusion</td></tr>
</table></td><td class="dcell"> <span style="font-variant:small-caps">rule_name</span></td></tr>
</table></td></tr>
</table>
    </div>
</div>
which means that when <span style="font-style:italic">condition</span><sub>1</sub>, ..., and <span style="font-style:italic">condition</span><sub>n</sub> are fulfilled, then <span style="font-style:italic">conclusion</span> holds. As expected, <span style="font-variant:small-caps">rule_name</span> is the name of the rule. What is expressed in the conditions and in the conclusion depends on whether it is a typing or a semantics rule.
</p>

{{ macros.linkable_header(3, 'Typing Rules', 'typing-rules', 'section') }}

<p>The typing rules borrow the concept of <em>judgement</em>, which is a notation found in logic to concisely express a proposition under some hypotheses, also called the <em>environment</em>. The general form of judgement when typing Michelson instructions is
Γ ⊢ <span style="font-style:italic">instr</span> :: <span style="font-style:italic">A</span> ⇒ <span style="font-style:italic">B</span>,
which can be read as follows: under the typing environment Γ, the Michelson instruction <span style="font-style:italic">instr</span> expects a stack of type <span style="font-style:italic">A</span>, and transforms it into a stack of type <span style="font-style:italic">B</span>. A stack type is a sequence of types, where the leftmost type of the sequence is the type for the top value of the stack.
</p>

<p>The conditions of a typing rule are usually judgements (but not always, they can be any kind of mathematical condition), and the conclusion is also a judgement.</p>

<p>For instance, the <span style="font-variant:small-caps">IF</span></td> rule below:
<div class="rules typing">
    <div class="rule-box">
<table class="display dcenter"><tr style="vertical-align:middle"><td class="dcell"><table class="display"><tr style="vertical-align:middle"><td class="dcell"><table class="display"><tr><td class="dcell" style="text-align:center"><table class="cellpading0" style="border-spacing:6px;border-collapse:separate;"><tr><td style="text-align:left;white-space:nowrap">  Γ  ⊢  <span style="font-style:italic">instr</span><sub>1</sub>  ::  <span style="font-style:italic">A</span>  ⇒  <span style="font-style:italic">B</span>  </td></tr>
<tr><td style="text-align:left;white-space:nowrap">  Γ  ⊢  <span style="font-style:italic">instr</span><sub>2</sub>  ::  <span style="font-style:italic">A</span>  ⇒  <span style="font-style:italic">B</span>  </td></tr>
</table></td></tr>
<tr><td class="hrule"></td></tr>
<tr><td class="dcell" style="text-align:center">
Γ  ⊢  <span style="font-weight:bold"><span style="font-style:italic">IF</span></span>   <span style="font-style:italic">instr</span><sub>1</sub>   <span style="font-style:italic">instr</span><sub>2</sub>  ::  <span style="font-weight:bold"><span style="font-style:italic">bool</span></span>  :  <span style="font-style:italic">A</span>  ⇒  <span style="font-style:italic">B</span> </td></tr>
</table></td><td class="dcell"> <span style="font-variant:small-caps">IF</span></td></tr>
</table></td></tr>
</table>
    </div>
</div>
means that under a typing environment Γ, if both the <span style="font-style:italic">instr</span><sub>1</sub> and <span style="font-style:italic">instr</span><sub>2</sub> Michelson instructions expect a stack of type <span style="font-style:italic">A</span> and return a stack of type <span style="font-style:italic">B</span>, then the <span style="font-weight:bold"><span style="font-style:italic">IF</span></span> <span style="font-style:italic">instr</span><sub>1</sub> <span style="font-style:italic">instr</span><sub>2</sub> instruction expects a stack of type <span style="font-weight:bold"><span style="font-style:italic">bool</span></span> : <span style="font-style:italic">A</span> and returns a stack of type <span style="font-style:italic">B</span>. The intent behind the values of type <span style="font-weight:bold"><span style="font-style:italic">bool</span></span>, <span style="font-style:italic">A</span> and <span style="font-style:italic">B</span> is not expressed in a typing rule, it is the purpose of the semantics rule.
</p>

{{ macros.linkable_header(3, 'Semantics Rules', 'semantics-rules', 'section') }}

<p>Semantics rules rely on a synctactic convention, whose general form is <span style="font-style:italic">i</span> / <span style="font-style:italic">S</span> ⇒ <span style="font-style:italic">S</span><span style="font-style:italic">′</span>, and which means that when applying the instruction <span style="font-style:italic">i</span> on the stack <span style="font-style:italic">S</span>, the result is the stack <span style="font-style:italic">S</span><span style="font-style:italic">′</span>.</p>

<p>As for typing rules, the conditions of a semantics rule are usually of the above form (but not always, they can be any kind of mathematical condition), and the conclusion also follows this syntax.</p>

<p>For instance, consider the two rules below:
<div class="rules semantics">
    <div class="rule-box">
<table class="display dcenter"><tr style="vertical-align:middle"><td class="dcell"><table class="display"><tr style="vertical-align:middle"><td class="dcell"><table class="display"><tr><td class="dcell" style="text-align:center"><table class="cellpading0" style="border-spacing:6px;border-collapse:separate;"><tr><td style="text-align:left;white-space:nowrap">  <span style="font-style:italic">i</span><sub>1</sub>  /  <span style="font-style:italic">S</span>  ⇒  <span style="font-style:italic">S</span><span style="font-style:italic">′</span>  </td></tr>
</table></td></tr>
<tr><td class="hrule"></td></tr>
<tr><td class="dcell" style="text-align:center">
<span style="font-weight:bold"><span style="font-style:italic">IF</span></span>   <span style="font-style:italic">i</span><sub>1</sub>   <span style="font-style:italic">i</span><sub>2</sub>  /  <span style="font-weight:bold"><span style="font-style:italic">True</span></span>  :  <span style="font-style:italic">S</span>  ⇒  <span style="font-style:italic">S</span><span style="font-style:italic">′</span> </td></tr>
</table></td><td class="dcell"> <span style="font-variant:small-caps">IF__tt</span></td></tr>
</table></td></tr>
</table>

<table class="display dcenter"><tr style="vertical-align:middle"><td class="dcell"><table class="display"><tr style="vertical-align:middle"><td class="dcell"><table class="display"><tr><td class="dcell" style="text-align:center"><table class="cellpading0" style="border-spacing:6px;border-collapse:separate;"><tr><td style="text-align:left;white-space:nowrap">  <span style="font-style:italic">i</span><sub>2</sub>  /  <span style="font-style:italic">S</span>  ⇒  <span style="font-style:italic">S</span><span style="font-style:italic">′</span>  </td></tr>
</table></td></tr>
<tr><td class="hrule"></td></tr>
<tr><td class="dcell" style="text-align:center">
<span style="font-weight:bold"><span style="font-style:italic">IF</span></span>   <span style="font-style:italic">i</span><sub>1</sub>   <span style="font-style:italic">i</span><sub>2</sub>  /  <span style="font-weight:bold"><span style="font-style:italic">False</span></span>  :  <span style="font-style:italic">S</span>  ⇒  <span style="font-style:italic">S</span><span style="font-style:italic">′</span> </td></tr>
</table></td><td class="dcell"> <span style="font-variant:small-caps">IF__ff</span></td></tr>
</table></td></tr>
</table>
    </div>
</div>
<span style="font-variant:small-caps">IF__tt</span> means that the result of executing <span style="font-weight:bold"><span style="font-style:italic">IF</span></span> <span style="font-style:italic">i</span><sub>1</sub> <span style="font-style:italic">i</span><sub>2</sub> on the stack <span style="font-weight:bold"><span style="font-style:italic">True</span></span> : <span style="font-style:italic">S</span> is exactly the result of executing <span style="font-style:italic">i</span><sub>1</sub> on <span style="font-style:italic">S</span>.<br/>
And similarly, <span style="font-variant:small-caps">IF__ff</span> means that the result of executing <span style="font-weight:bold"><span style="font-style:italic">IF</span></span> <span style="font-style:italic">i</span><sub>1</sub> <span style="font-style:italic">i</span><sub>2</sub> on the stack <span style="font-weight:bold"><span style="font-style:italic">False</span></span> : <span style="font-style:italic">S</span> is exactly the result of executing <span style="font-style:italic">i</span><sub>2</sub> on <span style="font-style:italic">S</span>.
</p>

{{ macros.linkable_header(2, 'Types', 'types', 'section') }}

Michelson data types (and by extension, the values of each type) can be
characterized by the following type attributes:

<dl>
<dt>Comparable (C)</dt>
<dd>Values of comparable types can be compared using the <code>COMPARE</code> instruction, can be stored as elements in <code>set</code>s, can index <code>map</code>s and <code>big map</code>s, and can be used as <code>ticket</code> payloads.</dd>
<dt>Passable (PM)</dt>
<dd>Values of passable types can be taken as a parameter in contracts.</dd>
<dt>Storable (S)</dt>
<dd>Values of storable types are allowed to appear inside the storage of contracts.</dd>
<dt>Pushable (PU)</dt>
<dd>Literal values of pushable types can be given as parameter to the <code>PUSH</code> instruction. Values of pushable types can also be emmitted using the <code>EMIT</code> instruction, returned as error value using the <code>FAILWITH</code> instruction, and deserialized using the <code>UNPACK</code> instruction.</dd>
<dt>Packable (PA)</dt>
<dd>Values of packable types can be serialized using the <code>PACK</code> instruction.</dd>
<dt><code>big_map</code> value (B)</dt>
<dd><code>big_map</code> values can be stored as values in <code>big_map</code>s.</dd>
<dt>Duplicable (D)</dt>
<dd>Literal values of duplicable types can be given as parameter to the <code>DUP</code> instruction.</dd>
</dl>

For compound types, the value of an attribute may depend on the value
of the same attribute on subtypes. For example, the type <code>map kty
vty</code> is never comparable and it is storable exactly
when <code>vty</code> is storable. The following table documents the
value of all type attributes for all Michelson
types; <span class="check" style="padding:4px">✔</span> means that the attribute always
holds for the given type, <span class="cross" style="padding:4px">✘</span> means that the
attribute never holds for the given type,
and <span class="maybe" style="padding:4px"><code>ty_1</code>...<code>ty_n</code></span>
means that the attribute holds exactly when the attribute holds for all the
<code>ty_i</code> subtypes.

{{ macros.linkable_item('The attributes of each type are given in the table below.', 'types-table', 'table') }}
<div class="table-container">
    {{ macros.quick_ref_tbl_ty(lang_def.get_types(), lang_def.get_type_attributes()) }}
</div>

All pushable types are also packable but <code>contract</code> is
packable without being pushable. Except for the domain specific
types <code>operation</code>, <code>contract</code>,
<code>ticket</code>, <code>sapling_state</code> and <code>big_map</code>, all
types are passable, storable, pushable, packable and can be used as values in
<code>big_map</code>s. The only type that is not duplicable is <code>ticket</code>.

<!-- <script src="js/scripts.js"></script> -->
<!-- <h2>Overview</h2> -->
{{ macros.linkable_header(2, 'Instructions', 'instructions', 'section') }}
<div class="table-container">{{ macros.quick_ref_tbl(lang_def.get_instructions()) }}</div>

{{ macros.linkable_header(2, 'Instructions by Category', 'instructions-by-category', 'section') }}
{% for cat, title in lang_def.get_categories().items() %}
{{ macros.linkable_header(3, title, 'instructions-' ~ cat, 'section') }}
<div class="table-container">
    {{ macros.quick_ref_tbl(lang_def.get_instructions_by_category(cat)) }}
</div>
{% endfor %}

{{ macros.linkable_header(2, 'Type Reference', 'type-reference', 'section') }}

{% for type in lang_def.get_types() %}
{% include 'type.html' %}
{% endfor %}

{{ macros.linkable_header(2, 'Instruction Reference', 'instruction-reference', 'section') }}

{% for instr in lang_def.get_instructions() %}
{% include 'instruction.html' %}
{% endfor %}
