import yaml
import json
import sys
import os.path
from jsonschema import validate
import argparse


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Validate json or yaml file against json schema.')
    parser.add_argument('schema', metavar='schema.json', type=argparse.FileType('r'), help='the schema file')
    parser.add_argument('data_files', metavar='data.[json|yaml]', type=argparse.FileType('r'), nargs='+',
                        help='the data file(s)')
    args = parser.parse_args()

    with args.schema as schema_file:
        schema = json.load(schema_file)

        for df in args.data_files:
            with df as df:
                ext = os.path.splitext(df.name)[1]
                if ext == '.yaml':
                    data = yaml.safe_load(df)
                elif ext == '.json':
                    data = json.load(df)
                else:
                    raise Exception(f'Unrecognized data-file extension {ext}')
                validate(data, schema=schema)
                print(f'{df.name} validates against {schema_file.name}')
