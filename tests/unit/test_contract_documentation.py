"""This module contains a suite of tests that verifies that the type
attributes specified in michelson_reference/michelson-meta.yaml are
coherent with the Michelson interpreter of the Tezos node (tests test_type_*).

Also, the test test_examples verifies that each example given in
michelson_reference/michelson-meta.yaml executes according to the
initial_storage, input and the expected final_storage specified in the same
file. That is, it executes the example with the initial_storage and input
using the Michelson interpreter, and compares the obtained final storage with
the expected final_storage.

"""
import sys
from typing import List, Tuple, Any, Optional
from typing_extensions import TypedDict

import pytest

from michelson_reference.language_def import LanguageDefinition

from tests.tools.paths import TEZOS_CONTRACTS
from tests.tools.utils import assert_run_failure

from tests.client.client import Client


def get_ldef() -> LanguageDefinition:
    """Constructs and returns a language definition."""
    return LanguageDefinition(
        'michelson_reference/michelson-meta.yaml',
        'michelson_reference/michelson-meta-schema.json',
        'michelson_reference/michelson.json',
        TEZOS_CONTRACTS
    )


# TODO: The definition of types returned by LanguageDefinition should be moved
# to the file where LanguageDefinition is defined, which should be done when
# mypy is applied to the complete project.

TyDescr = TypedDict('TyDescr', {  # pylint: disable=invalid-name
    'ty': str,
    'ty_args': str,
    'related_ops': List[str],
    'documentation_short': str,
    'comparable': bool,
    'pushable': bool,
    'passable': bool,
    'storable': bool,
    'packable': bool,
    'unpackable': bool,
    'duplicable': bool,
    'examples': List[Tuple[str, str]],
    'insertions': List[str],
    'big_map_value': bool,
    'documentation': str
})


InstructionExample = Tuple[str, str, str, str, Optional[List[str]]]


ContractParam = TypedDict('ContractParam', {  # pylint: disable=invalid-name
    'code': str,
    'parameter': str
})


def all_types() -> List[TyDescr]:
    """Returns all type descriptions from the language definition."""
    types = get_ldef().get_types()
    for ty_descr in types:
        typ = ty_descr['ty']
        assert ty_descr['insertions'], \
            f'Ensure we have instantiations of parametric types; {typ} missing'
    return types


# mypy cannot figure out the return type of this function, so a more
# general type is given.
def all_type_examples() -> List[Tuple[Any, ...]]:
    types = all_types()
    exs = []
    for ty_descr in types:
        for val in ty_descr['examples']:
            exs.append(tuple(val) + (ty_descr,))
    return exs


def all_instruction_examples() -> List[InstructionExample]:
    """Retrieves all example contracts from the language definitions.

    The path of each contract is returned, along with the expected
    final storage, given an initial storage and input and an optional
    set of parameters that should be passed to `octez-client run
    script ...`.

    """
    examples = []
    ldef = get_ldef()
    for instr in ldef.get_instructions():
        if 'examples' not in instr:
            continue

        for ex in instr['examples']:
            if 'path' not in ex:
                print(f"Missing path for example: {ex}")
                sys.exit(1)
            if 'input' not in ex:
                print(f"Missing initial input for example: {ex}")
                sys.exit(1)
            if 'client_run_script_params' not in ex:
                params = None
            else:
                params = ex['client_run_script_params']
            examples.append((ex['path'], ex['initial_storage'],
                             ex['input'], ex['final_storage'], params))

    return examples


def contract(parameter_ty: str, storage_ty: str, code: str) -> str:
    """Constructs a Michelson script as a string.

    Constructs a Michelson script as a string given a parameter type, storage
    type and an instruction sequence."""
    return (f'parameter ({parameter_ty}); ' +
            f'storage ({storage_ty}); ' +
            f'code {{ {code} }}')


def contract_with_val_on_stack(
        ty_descr: TyDescr,
        code: str,
        storage_ty: str) -> ContractParam:
    """Constructs a Michelson script that executes code with a
    value of the given type on the stack.

    Constructs a Michelson script of storage type storage_ty that pushes a
    value of the type described by ty_descr on to the stack, and then executes
    code. Code must be a michelson instruction of the following type, where ty
    is ty_descr['ty']:

      code :: ty : []  ->  pair (list operation) storage_ty : []

    """

    assert ty_descr['insertions']
    typ = ty_descr['insertions'][0]
    value = None

    if ty_descr['examples']:
        (value, typ) = ty_descr['examples'][0]

    if ty_descr['passable'] and value is not None:
        code = contract(
            typ, storage_ty,
            f'CAR; {code}')
        parameter = value
    elif ty_descr['pushable'] and value is not None:
        code = contract(
            'unit', storage_ty,
            f'DROP; PUSH ({typ}) ({value}); {code}')
        parameter = 'Unit'
    else:
        code = contract(
            'unit', storage_ty,
            f'DROP; NONE ({typ}); ASSERT_SOME; {code}')
        parameter = 'Unit'
    return {
        'code': code,
        'parameter': parameter
    }


def contract_with_2vals_on_stack(
        ty_descr: TyDescr,
        code: str,
        storage_ty: str) -> ContractParam:
    """Constructs a Michelson script that executes code with two
    values of the given type on the stack.

    Constructs a Michelson script of storage type storage_ty that pushes two
    values of the type described by ty_descr on to the stack, and then executes
    code. Code must be a michelson instruction of the following type, where ty
    is ty_descr['ty']:

      code :: ty : ty : []  ->  pair (list operation) storage_ty : []

    """
    if ty_descr['duplicable']:
        return contract_with_val_on_stack(
            ty_descr,
            f'DUP; {code}',
            storage_ty
        )
    if ty_descr['ty'] == 'ticket':
        code = contract(
            'unit', storage_ty,
            'DROP; PUSH nat 4; PUSH bytes 0x01; TICKET; ' +
            f'PUSH nat 4; PUSH bytes 0x01; TICKET; {code}')
        parameter = 'Unit'
        return {
            'code': code,
            'parameter': parameter
        }
    assert False, "We assume that the only non-duplicable type is 'ticket'."
    return None


def assert_type_checks(client: Client,
                       script: str,
                       should_type_check: bool,
                       error_pattern: str) -> None:
    """Verifies the result of typechecking script according to
    should_type_check and error_pattern.

    If should_type_check is True, then it asserts that script typechecks.
    If should_type_check is False, then it asserts typechecking fails with
    an error message matching error_pattern.
    """
    def cmd():
        client.typecheck(script, file=False)

    if should_type_check:
        cmd()
    else:
        with assert_run_failure(error_pattern):
            cmd()


class TestContractsDocumentation:

    @pytest.mark.parametrize('ty_descr', all_types())
    def test_type_passability(self, client: Client, ty_descr: TyDescr) -> None:
        """Verify coherency of the passable flag in the meta-data.

        For each type ty (as given by ty_descr), this test constructs a script
        of parameter type ty.
        If the type is marked as passable, we assert that the script
        typechecks. If not marked as such, we assert that the script does not
        typecheck.
        """

        tc_contract = contract(ty_descr['insertions'][0],
                               'unit',
                               'DROP; UNIT; NIL operation; PAIR')

        assert_type_checks(client, tc_contract, ty_descr['passable'],
                           r'script .* is ill-typed')

    @pytest.mark.parametrize('ty_descr', all_types())
    def test_type_storability(self, client: Client, ty_descr: TyDescr):
        """Verify coherency of the storability flag in the meta-data.

        For each type ty (as given by ty_descr), this test constructs a script
        of storage type ty.
        If the type is marked as passable, we assert that the script
        typechecks. If not marked as such, we assert that the script does not
        typecheck.
        """

        store_contract = contract_with_val_on_stack(
            ty_descr,
            'NIL operation; PAIR',
            ty_descr['insertions'][0]
        )

        assert_type_checks(client, store_contract['code'],
                           ty_descr['storable'], r'script .* is ill-typed')

    @pytest.mark.parametrize('ty_descr', all_types())
    def test_type_comparability(self, client: Client, ty_descr: TyDescr):
        """Verify coherency of the comparable flag in the meta-data.

        For each type (as given by ty_descr), this test constructs a script
        with a COMPARE executed on a stack with two values of that type.  If
        the type is marked as comparable, we assert that the script
        typechecks. If not marked as such, we assert that the script does not
        typecheck.
        """

        cmp_contract = contract_with_2vals_on_stack(
            ty_descr, 'COMPARE; DROP; ' +
            'UNIT; NIL operation; PAIR',
            'unit'
        )

        assert_type_checks(client, cmp_contract['code'],
                           ty_descr['comparable'], 'comparable type expected')

    @pytest.mark.parametrize('_val, _ty, ty_descr', all_type_examples())
    def test_type_comparability_in_structures(self,
                                              client: Client,
                                              _val: str,
                                              _ty: str,
                                              ty_descr: TyDescr):
        """Verify coherency of the comparability flag w.r.t. structures in
        the meta-data.

        For each type ty (as given by ty_descr), this test constructs a script
        that constructs a set, a map, and a big_map storing that type.
        If the type is marked as comparable, we assert that the script
        typechecks. If not marked as such, we assert that the script does not
        typecheck.
        """

        tc_contract = contract(
            'unit', 'unit',
            'DROP; ' +
            'EMPTY_SET (' + ty_descr['insertions'][0] + '); DROP; ' +
            'EMPTY_MAP (' + ty_descr['insertions'][0] + ') nat; DROP; ' +
            'EMPTY_BIG_MAP (' + ty_descr['insertions'][0] + ') nat; DROP; ' +
            'UNIT; NIL operation; PAIR')

        assert_type_checks(client, tc_contract, ty_descr['comparable'],
                           r'script .* is ill-typed')

    @pytest.mark.parametrize('ty_descr', all_types())
    def test_type_pushability(self, client: Client, ty_descr: TyDescr):
        """Verify coherency of the pushability flag in the meta-data.

        For each type ty (as given by ty_descr), this test constructs scripts
        that use instructions which require the type to be pushable.
        If the type is marked as pushable, we assert that the scripts
        typecheck. If not marked as such, we assert that the script do not
        typecheck.
        """

        if not ty_descr['examples']:
            pytest.skip("no example values")

        (value, typ) = ty_descr['examples'][0]

        push_contract = contract(
            'unit', 'unit',
            f'DROP; PUSH ({typ}) {value}; DROP; ' +
            'UNIT; NIL operation; PAIR')

        assert_type_checks(client, push_contract, ty_descr['pushable'],
                           r'script .* is ill-typed')

        emit_contract1 = contract(
            'unit', 'unit',
            f'NONE ({typ}); ASSERT_SOME; EMIT; DROP; ' +
            'CAR; NIL operation; PAIR')

        assert_type_checks(client, emit_contract1, ty_descr['pushable'],
                           r'script .* is ill-typed')

        # Same with a type argument on EMIT
        emit_contract2 = contract(
            'unit', 'unit',
            f'NONE ({typ}); ASSERT_SOME; EMIT ({typ}); DROP; ' +
            'CAR; NIL operation; PAIR')

        assert_type_checks(client, emit_contract2, ty_descr['pushable'],
                           r'script .* is ill-typed')

        failwith_contract = contract(
            'unit', 'unit',
            f'NONE ({typ}); ASSERT_SOME; FAILWITH')

        assert_type_checks(client, failwith_contract, ty_descr['pushable'],
                           r'script .* is ill-typed')

        unpack_contract = contract(
            'bytes', 'unit',
            f'UNPAIR; UNPACK ({typ}); DROP; ' +
            'NIL operation; PAIR')

        assert_type_checks(client, unpack_contract, ty_descr['pushable'],
                           r'script .* is ill-typed')

    @pytest.mark.parametrize('ty_descr', all_types())
    def test_type_packability(self, client: Client, ty_descr: TyDescr):
        """Verify coherency of the packability flag in the meta-data.

        For each type ty (as given by ty_descr), this test constructs a script
        that attempts to PACK a value of that type.
        If the type is marked as packable, we assert that the script
        typechecks. If not marked as such, we assert that the script does not
        typecheck.
        """

        pack_contract = contract_with_val_on_stack(
            ty_descr, 'PACK; DROP; UNIT; NIL operation; PAIR',
            'unit'
        )

        assert_type_checks(client, pack_contract['code'], ty_descr['packable'],
                           r'script .* is ill-typed')

    @pytest.mark.parametrize('ty_descr', all_types())
    def test_type_dupability(self, client: Client, ty_descr: TyDescr):
        """Verify coherency of the dupability flag in the metadata.

        For each type ty (as given by ty_descr), this test constructs a script
        that attempts to DUP a value of that type.
        If the type is marked as duplicable, we assert that the script
        typechecks. If not marked as such, we assert that the script does not
        typecheck.
        """

        dup_contract = contract_with_val_on_stack(
            ty_descr, 'DUP; DROP 2; UNIT; NIL operation; PAIR',
            'unit'
        )

        assert_type_checks(client, dup_contract['code'],
                           ty_descr['duplicable'],
                           r'script .* is ill-typed')

    @pytest.mark.parametrize('ty_descr', all_types())
    def test_type_big_map_storability(self, client: Client, ty_descr: TyDescr):
        """Verify coherency of the big map value flag in the meta-data.

        For each type ty (as given by ty_descr), this test constructs a script
        that constructs a big_map storing that type.
        If the type is marked as being storable in a big map, we assert that
        the script typechecks. If not marked as such, we assert that the script
        does not typecheck.
        """

        tc_contract = contract('unit', 'unit',
                               'DROP; EMPTY_BIG_MAP nat (' +
                               ty_descr['insertions'][0] + ');' +
                               'DROP; UNIT; NIL operation; PAIR')

        assert_type_checks(client, tc_contract, ty_descr['big_map_value'],
                           r'script .* is ill-typed')

    @pytest.mark.parametrize('ty_descr', all_types())
    def test_type_passable_pushables_can_be_applied(
            self, client: Client, ty_descr: TyDescr):
        """Verify coherency of the passable and pushable flag in the
        meta-data w.r.t. the APPLY instruction.

        For each type typ (as given by ty_descr), this test constructs a
        script that with a lambda of type

           lambda (pair typ unit) unit

        and applies it to a value of type typ with APPLY.

        Only values that are passable and pushable can be partially applied in
        this way. If the type is marked as being both passable and pushable,
        we assert that the script typechecks. If not marked as such, we assert
        that the script does not typecheck.
        """

        typ = ty_descr['insertions'][0]
        apply_contract = contract_with_val_on_stack(
            ty_descr,
            (f'LAMBDA (pair ({typ}) unit) unit {{ CDR; }}; '
             'SWAP; APPLY; UNIT; EXEC; NIL operation; PAIR'),
            'unit'
        )

        should_typecheck = ty_descr['passable'] and ty_descr['pushable']
        assert_type_checks(client, apply_contract['code'], should_typecheck,
                           r'script .* is ill-typed')

    @pytest.mark.parametrize(
        'path, initial_storage, inp, final_storage, params',
        all_instruction_examples())
    def test_examples(self,
                      client: Client,
                      path: str,
                      initial_storage: str,
                      inp: str,
                      final_storage: str,
                      params: List[str]):
        """Verifies that the example contracts in the language definition
        executes according to the specified initial_storage, input and
        final_output.

        """
        run_script_output = client.run_script(
            path, initial_storage, inp, params=params)
        assert run_script_output.storage == final_storage
